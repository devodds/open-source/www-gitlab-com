- name: Age of current open application and container vulnerabilities by severity
  base_path: "/handbook/security/performance-indicators/"
  definition: The age of current open vulnerabilities gives us an at the moment snapshot in time of how fast we are scheduling and fixing the vulnerabilities found post-Production deploy. The age is measured in days, and the targets for each severity are defined in the <a href="https://about.gitlab.com/handbook/security/#severity-and-priority-labels-on-security-issues">Security Handbook</a>. For Security purposes, please view this chart directly in Sisense.
  target: <a href="https://about.gitlab.com/handbook/security/#severity-and-priority-labels-on-security-issues">Time to remediate</a>

  urls:
    - https://app.periscopedata.com/app/gitlab/758795/Appsec-Embedded-Dashboard

  org: Security Department
  is_key: true
  health:
    level: -1
    reasons:
    - There has been an increase in scanner findings from automation which has resulted in a slight increase of the number of S1s and S2s. Despite this increase in volume, we continue to see sustained open vulnerability age below SLA targets for all severities. This KPI is currently considered healthy.

- name: Security Engineer On-Call Page Volume
  base_path: "/handbook/security/performance-indicators/"
  definition: This metric is focused around the volume and severity of paged incidents
    to the Security Engineer On-Call. This data can be used to track and identify trends associated with disruption work, which if and when possible, should be minimized. For Security purposes, please view this chart directly in Sisense.
  target: Number of pages/month does not exceed +50% of monthly average of the last 12 months for 3 consecutive months
  org: Security Department
  is_key: true
  health:
    level: -1
    reasons:
    - A 4% decrease in on-call pages was experienced over the last 3 months as compared to the last 12 months. Notably, this shows continued stability in the on-call page volume.  The number of S1 paged incidents is one over the last 3 months, compared to .5 S1 page per month over the last 12 months average. These volumes are well within the acceptable threshold and we attribute this stability to security improvements made through rapid action groups and working groups.
    - Short term fluctuations are to be expected. Long term trends should be identified and actions should be taken to correct negative trends and to continue promoting positive trends.
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9217413&udv=0

- name: Security Control Risk by System
  base_path: "/handbook/security/performance-indicators/"
  definition: Security Compliance performs regular testing of controls for in scope systems and uses the [System Risk Score](https://about.gitlab.com/handbook/security/security-assurance/system-risk-score.html) methodology to determine the system risk of each. A System risk rating of 1 means all controls evaluated for that system are fully effectively (very low risk) and there are no open observations associated with that system and the higher the system risk score the more risk that particular system carries.
  target: Confidential
  org: Security Department
  is_key: true
  health:
    level: -1
    reasons:
    - Security Compliance performs regular testing of controls by system and uses the [System Risk Scoring methodology](https://about.gitlab.com/handbook/security/security-assurance/system-risk-score.html) to determine the overall risk of each system in scope of GitLab’s compliance and regulatory programs. A system risk score of 1 means all controls evaluated for that system are operating fully effectively with no open observations and a higher system risk score means there are an increasing number of observations opened against that system that require remediation. 54% (or 13 out of 24) systems assessed met the target. There are 151 observations open for these 24 systems. 
  urls:
    - https://app.periscopedata.com/app/gitlab/977899/Security-Impact?widget=14446739&udv=0

- name: Security Impact on Net ARR 
  base_path: "/handbook/security/performance-indicators/"
  definition: The Field Security organization functions as a sales and customer enablement team therefore a clear indicator of success is directly reflected in the engagement of their assessment services by Legal, Sales, TAMs and customers themselves. Assessment services include completing security questionnaires, participating in customer calls, creating and providing security documentation, and facilitating customer audits. The dashboard is calculated as (assessments completed by customer + total contract value by customer = monthly dollar security impact).
  target: Confidential
  org: Security Department
  is_key: true
  health:
    level: -1
    reasons:
    - Field Security observed an increase in customer assurance requests and net ARR impact in May 2023. The increase in requests resulted in the largest number of requests processed since inception of this KPI. Field Security observed a 35% increase in net ARR impact and a 2% increase in requests received compared to previous highs.

  urls:
    - https://app.periscopedata.com/app/gitlab/977899/Security-Impact?widget=13710204&udv=0

- name: Estimated Cost of Abuse
  base_path: "/handbook/security/performance-indicators/"
  definition: This metric tracks the estimated cost of abuse in terms of CI Compute Cost & Storage costs from blocked accounts. It also includes aggregated Networking Cost data when it is over the baseline spend for known skus that only trend up during periods of elevated abuse, although this is not tracked at the user level. It does not include reputation damage costs or labor costs of having to manually prevent certain types of abuse.
  target: less than $10K/Month
  org: Security Department
  is_key: true
  public: false
  health:
    level: -1
    reasons:
    - Confidential metric - See notes in Key Review agenda
  urls:
    - https://app.periscopedata.com/app/gitlab/780726/Estimated-Cost-of-Abuse
- name: Security Budget Plan vs Actuals
  base_path: "/handbook/security/performance-indicators/"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful. For Security purposes, please view this chart directly in Sisense.
    <a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11464">Latest data is in Adaptive, data team importing to Sisense in FY22Q2</a>
  target: See Sisense for target
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - This chart was recently updated and now reflective of current state. Security are within budget expectations.
  urls:
    - https://app.periscopedata.com/app/gitlab/633239/Security-Non-Headcount-BvAs

- name: Security Handbook MR Rate
  base_path: "/handbook/security/performance-indicators/"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching `/source/handbook/engineering/security` over time.
  target: 1
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Security was highly focused on operations and hiring in FY23. Action =  Security expect to see a marked increase in handbook updates once project work picks back up.
  sisense_data:
    chart: 10642322
    dashboard: 621064
    shared_dashboard: feac7198-86db-480b-9eae-c41cb479a209
    embed: v2

- name: Security Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Security Department
  is_key: true
  public: false
  health:
    level: -1
    reasons:
    - Action = Security leadership have created OKRs focused on addressing team member concerns contributing to attrition rates.
  urls:
    - "https://app.periscopedata.com/app/gitlab/862338/Security-Department-Retention"

- name: Security Average Age of Open Positions
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the average time job openings take from open to close. This metric includes sourcing time of candidates compared to Time to Hire or Time to Offer Accept which only measures the time from when a candidate applies to when they accept.
  target: at or below 50 days
  org: Security Department
  is_key: true
  public: true
  health:
    level: 1
    reasons:
    - High demand for Security professionals has lead to an extremely competitive hiring market. Security leadership are actively involved in the recruiting processes to reduce burden on recruiters and shorten time to hire.
  sisense_data:
      chart: 11885848
      dashboard: 872394
      embed: v2
      filters:
          - name: Department
            value: Security

- name: Security Department Discretionary Bonus Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Security had observed a downward trend for bonus nominations recently. The previous action for Managers to actively encourage team members to submit nominations where appropriate was effective at bringing the metric back on target.
  sisense_data:
    chart: 11860249
    dashboard: 873088
    embed: v2
    filters:
      - name: Breakout
        value: Department
      - name: Breakout_Division_Department
        value: Engineering - Security
        
- name: Security Incidents by Category
  base_path: "/handbook/security/performance-indicators/"
  definition: The metric groups security incidents by incident category and provides visibility into possible trends in attack types or targeted systems. Tracking of this metrics allows GitLab to adjust security control strategies, identify opportunities for improvements, and address security controls needing attention. For Security purposes, please view this chart directly in Sisense.
  target: Number of security incidents in any category does not exceed +50% of the individual category's 3-month average.
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - While the total number of security incidents is 19% greater than the 12 month average over the last 3 months, all incident categories are within expected and acceptable thresholds. We believe this higher number of incidents is due to higher fidelity detection, alerting and improved security awareness.
    - Category labels are applied in accordance to the [SIRT handbook page](https://about.gitlab.com/handbook/security/security-operations/sirt/). In the event that "NotApplicable" keeps growing, we'll be adding additional categories.
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9277541&udv=0

- name: Operational Security Risk Management (Tier 2 Risks)
  base_path: "/handbook/security/performance-indicators/"
  definition: Operational risk management enables organizations to proactively identify and mitigate operational security risks that may impact Organizational Output, Brand Reputation, Business Continuity, Customers & Stakeholders, Legal & Regulatory and/or Financials. This heatmap has been generated from ZenGRC. Numbers within each box indicate the total number of potential risks. Red boxes indicate the risk level is HIGH. Orange boxes indicate the risk level is MODERATE. Green boxes indicate the risk level is LOW. The heatmap shows risks that are currently open and accepted, in remediation or planned for remediation.
  target: TBD, this will be determined upon Sisense integration for detailed dashboarding
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - Security operational risk management enables organizations to proactively identify and mitigate operational security risks that may impact Organizational Output, Brand Reputation, Business Continuity, Customers & Stakeholders, Legal & Regulatory and/or Financials. This heatmap has been generated from ZenGRC. Numbers within each box indicate the total number of documented security operational risks. Red boxes indicate the risk level is HIGH. Orange boxes indicate the risk level is MODERATE. Green boxes indicate the risk level is LOW. The heatmap shows risks that are currently open and accepted, in remediation or planned for remediation. Security’s Annual Risk Assessment was completed in June and will be presented to the Board of Directors in their Q2 meeting. As part of the ARA and standard operational risk activities (such as risk treatment or trends in the IT environment), risk scores displayed in the heatmap are subject to change over time. Our reporting cadence will change from annual to quarterly in Q3 to drive risk treatment through iteration and transparency.
  sisense_data:
    chart: 11437860
    dashboard: 847984
    embed: v2

- name: Security Observations (Tier 3 Risks)
  base_path: "/handbook/security/performance-indicators/"
  parent: "/handbook/security/performance-indicators/#security-observations-tier-3-risks"
  definition: An indicator of information system and process risk, there are multiple inputs that lead to identification of Observations to include Security Compliance continuous control testing, third party (vendor) assessments, external audits and customer assessments.
  target: Confidential
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - 71% (or 805 of 1130) of all observations opened to date have been resolved, 114 of the 805 were high-risk observations. Of observations identified this fiscal year, ~28% (or 46 of 167) have been resolved. 33 high-risk observations have been identified.

  sisense_data:
    chart: 15739381
    dashboard: 1092210
    embed: v2

- name: Third Party Risk Management
  base_path: "/handbook/security/performance-indicators/"
  definition: An indicator of third party risk, third party risk assessments proactively identify potential vendor security risks as part of onboarding or contracting, enabling business owners to make risk based decisions throughout the vendor lifecycle.
  target: TBD, this will be determined upon Sisense integration for detailed dashboarding
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - TPRM has fielded 236 vendor intake requests fiscal year to date (5.4% YoY increase). These requests have resulted in 70 new third party assessments which is a -12.5% decrease YoY.
  sisense_data:
    chart: 11439229
    dashboard: 847984
    embed: v2

- name: Security Automation Iteration Velocity Average
  base_path: "/handbook/security/performance-indicators/"
  definition: We attempt to complete 7 weighted issues or more every two weeks. The measurement indicates how well the Security Automation team is scoping iterations over the last 4 biweekly iterations and provides a view of the average team velocity.
  target: 7
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - This PI is considered healthy.
  sisense_data:
    chart: 11480449
    dashboard: 850740
    shared_dashboard: a9d1978c-e247-4299-a5e2-a34934c06feb
    embed: v2

- name: Security Department Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the <a href="https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Security have recently announced multiple promotions. Action = Security leadership are now executing Individual Development Plans as part of one on ones with a direct focus on career development.
  sisense_data:
    chart: 11860231
    dashboard: 873087
    embed: v2
    filters:
      - name: Breakout
        value: Department
      - name: Breakout_Division_Department
        value: Engineering - Security

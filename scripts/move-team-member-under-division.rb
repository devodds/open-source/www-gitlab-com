#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'yaml'
require 'fileutils'

Dir['data/team_members/person/**/*.yml'].each do |path|
  next if path.count('/') == 5

  member = YAML.load_file(path, permitted_classes: [Date])
  division = member['division']
  filename = File.basename(path)
  prefix = filename.chr

  next unless division

  directory = "data/team_members/person/#{division}/#{prefix}"

  FileUtils.mkdir_p(directory)
  FileUtils.mv(path, "#{directory}/#{filename}")
end
